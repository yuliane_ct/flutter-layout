import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    //style Text of title
    final tittleTextStyle = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontFamily: 'Roboto',
      letterSpacing: 0.5,
      fontSize: 20,
    );

    //style Text of iconList text
    final descTextStyle = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.w800,
      fontFamily: 'Roboto',
      letterSpacing: 0.5,
    );

    //row stars
    var stars = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(Icons.star, color: Colors.green[500], size: 12,),
        Icon(Icons.star, color: Colors.green[500], size: 12,),
        Icon(Icons.star, color: Colors.green[500], size: 12,),
        Icon(Icons.star, color: Colors.black, size: 12,),
        Icon(Icons.star, color: Colors.black, size: 12,),
      ],
    );

    //container gabungan dari stars + reviews
    final ratings = Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
            stars,
            Text(
              '170 Reviews',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w800,
                fontFamily: 'Roboto',
                letterSpacing: 0.2,
              ),
            ),
        ],
      ),
    );

    //Container untuk iconList
    // DefaultTextStyle.merge() allows you to create a default text
    final iconList = DefaultTextStyle.merge(
      style: descTextStyle,
      child: Container(
        padding: EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Column(
                children: [
                  Icon(Icons.kitchen, color: Colors.green[500]),
                  Text('PREP:'),
                  Text('25 min'),
                ],
              ),
            ),

            Expanded(
              child: Column(
                children: [
                  Icon(Icons.timer, color: Colors.green[500]),
                  Text('COOK:'),
                  Text('1 hr'),
                ],
              ),
            ),

            Expanded(
              child: Column(
                children: [
                  Icon(Icons.restaurant, color: Colors.green[500]),
                  Text('FEEDS:'),
                  Text('4-6'),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    //Countainer  Column kiri (title + keterangan + reviews + iconList)
    final leftColumn = Container(
      padding: EdgeInsets.fromLTRB(0, 20, 10, 15),
      child: Column(
        children: [
          Text('Strawberry Povlova', style: tittleTextStyle, textAlign: TextAlign.center,),
          Text(
            '\nPovlova is a dessert named after the Russian balerina Anne Povlova. Features a crisp crust and soft, light inside, topped with fruit and whipped cream.',
            textAlign: TextAlign.justify, ),
          ratings,
          iconList,
        ],
      ),
    );
    
    return MaterialApp(
      title: 'Flutter layout demo',
      home: Scaffold(
        resizeToAvoidBottomInset: false, // set it to false
        appBar: AppBar(
          title: Text('Flutter layout demo'),
        ),
        body: SingleChildScrollView(
        child: Center(
          child: Container(
            margin: EdgeInsets.all(10),
            child: Card(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 5, top: 10, bottom: 10),
                            child: leftColumn,
                          ),
                        ),

                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(right: 2, left: 2),
                            child: Image.asset('assets/images/cake.png'),
                          ),
                        ),

                      ],
                    ),
              ),
            ),
          ),
        ),
        ),
      );
  }
}
